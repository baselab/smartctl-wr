
# smartctl-wr

*Check common SMART values on all drives*

I have written this small and simple wrapper for `smartctl` because tired of issue dozens of commands or write ad-hoc `for` cycles on the fly.

## Really?
Useful on systems with many disks (> 3 ?) where you have already scheduled SMART test jobs.

Maybe useless if smartd is (well-)configured with active notifications (eg. mail).

## Systems

* Linux based - AFAICT it can be easily ported to any *nix OS

## Dependencies

* bash - but I've little exp. with other shells, so it may work anyway
* a recent util-linux suite (with `lsblk`)
* sudo - if not run by root
* [smartmontools](https://www.smartmontools.org/)

## Usage

```
[sudo] smartctl-wr [help]
```

Output (e.g.):

```
DISK  HOURS  SMART  HEALTH  ATA_ERR  OTHERR  LAST_TEST  RESULT
sda   2807   on     passed  -        -       2450       Short-offline-Completed-without-error
sdb   2789   on     passed  -        -       203        Extended-offline-Completed-without-error
```
